(in-package #:splitcsv)

(defun main ()
  "Entry function of the SPLITCSV binary."
  (let ((cmd-args (uiop:command-line-arguments)))
    (if (> (length cmd-args) 1)
	(let ((csv-file (car cmd-args))
	      (file-length (read-from-string (cadr cmd-args))))
	  (split-csv csv-file file-length))
	(format t "Please ensure that you're using the correct amount of arguments.~%"))))

(defun split-csv (csv line-count)
  "Split CSV into separate files of length LINE-COUNT, including headers."
  (with-open-file (f csv)
    (let* ((headers (read-line f))
	   (expected-line-count (- line-count 1))
	   (current-line 0)
	   (increment 0)
	   (output-file (open (create-new-file-name csv increment)
			      :direction :output
			      :if-exists :supersede
			      :if-does-not-exist :create)))
      (write-line headers output-file)
      (loop for line = (read-line f nil :eof)
	    until (eq line :eof)
	    do (progn
		 (when (= current-line expected-line-count)
		   (close output-file)
		   (incf increment)
		   (setf current-line 0)
		   (setf output-file (open (create-new-file-name csv increment)
					   :direction :output
					   :if-exists :supersede
					   :if-does-not-exist :create))
		   (write-line headers output-file))

		 (write-line line output-file)
		 (incf current-line)))
      (close output-file))))

(defun create-new-file-name (csv-filename current-increment)
  "Update CSV-FILENAME with the CURRENT-INCREMENT."
  (let* ((file-parts (uiop:split-string csv-filename :separator "."))
	 (file-name (format nil "~{~A~^.~}" (reverse (cdr (reverse file-parts)))))
	 (file-extension (car (reverse file-parts))))
    (format nil "~a_~a.~a" file-name current-increment file-extension)))

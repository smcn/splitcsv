;;;; splitcsv.asd

(asdf:defsystem #:splitcsv
  :description "Split the CSV file into separate files"
  :author "Stephen McNelly <stephenmcnelly@gmail.com>"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :build-operation "program-op"
  :build-pathname "binaries/splitcsv"
  :entry-point "splitcsv:main"
  :components ((:module "src"
		:components
		((:file "package")
		 (:file "splitcsv")))))

# splitcsv

Command line utility used to split the provided csv file into chunks.

Please find a prebuilt binary within the `binaries` directory.

Usage:

`splitcsv file-to-split.csv 10000`

## Build instructions

Tested with SBCL but will most likely work with CCL. Most likely you'll only need access to `uiop` to get this to build.

Clone this git project, `cd` into it, then run `make build`. You will see an executable binary, please place that within your `$PATH`.
